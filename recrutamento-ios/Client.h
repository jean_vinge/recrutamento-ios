//
//  Client.h
//  recrutamento-ios
//
//  Created by Jean Vinge on 13/07/15.
//  Copyright (c) 2015 Jean Vinge. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>

@interface Client : AFHTTPRequestOperationManager

+ (id)sharedClient;
+ (void)sharedClientWithURL:(NSURL *)URL;
- (BOOL)isConnected;

@end
