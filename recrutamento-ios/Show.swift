//
//  Show.swift
//  recrutamento-ios
//
//  Created by Jean Vinge on 13/07/15.
//  Copyright (c) 2015 Jean Vinge. All rights reserved.
//

import Foundation

class Show: NSObject {

    var showTitle:String?
    var showImageUrl:NSURL?
    
    func parseShowWithJson(json:NSDictionary) -> Show {
        
        var model : Show = Show()
        let url = json.objectForKey("images")?.objectForKey("poster")?.objectForKey("medium") as! String;
        
        model.showTitle = json.objectForKey("title") as? String
        model.showImageUrl = NSURL(string:url)
        
        return model
    }
    
    func parseListOfShows(array:NSArray) -> NSArray {
        
        var list = [Show]();
        
        for (index, object) in enumerate(array) {
            
            list.append(parseShowWithJson(object as! NSDictionary))
        }
        
        return list;
    }
    
}
