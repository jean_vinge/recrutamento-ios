//
//  ShowCustomCollectionCell.swift
//  recrutamento-ios
//
//  Created by Jean Vinge on 09/07/15.
//  Copyright (c) 2015 Jean Vinge. All rights reserved.
//

import UIKit

class ShowCustomCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var coverTitle: UILabel!
}
