//
//  Show.h
//  recrutamento-ios
//
//  Created by Jean Vinge on 13/07/15.
//  Copyright (c) 2015 Jean Vinge. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShowClient : NSObject

+ (void)returnPopularShowsWithCompletion:(void (^)(NSArray *list))completion failure:(void (^)(NSError *error))failure;
@end
