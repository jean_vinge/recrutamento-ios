//
//  Client.m
//  recrutamento-ios
//
//  Created by Jean Vinge on 13/07/15.
//  Copyright (c) 2015 Jean Vinge. All rights reserved.
//

#import "Client.h"

NSString *const kApiKey = @"891c81e9978f40474427c821cb9b6e0d1c0610ba4fabd58c9e1465c1988b8c02";
NSString *const kApiVersion = @"2";

@interface Client ()

@property (nonatomic) AFNetworkReachabilityStatus status;

@end

@implementation Client

static Client *clientInstance;

#pragma mark Singleton Methods

+ (id)sharedClient{
    
    return clientInstance;
}

+ (void)sharedClientWithURL:(NSURL *)URL {
    clientInstance = [[self alloc] initWithBaseURL:URL];
}

- (id)initWithBaseURL:(NSURL *)url {
    self = [super initWithBaseURL:url];
    
    if (self) {
        [self setResponseSerializer:[AFJSONResponseSerializer serializer]];
        [self setRequestSerializer:[AFJSONRequestSerializer serializer]];
        
        [self.requestSerializer setValue:kApiKey forHTTPHeaderField:@"trakt-api-key"];
        [self.requestSerializer setValue:kApiVersion forHTTPHeaderField:@"trakt-api-version"];
        
        [[AFNetworkReachabilityManager sharedManager] startMonitoring];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(reachabilityChanged:)
                                                     name:AFNetworkingReachabilityDidChangeNotification
                                                   object:nil];
    }
    
    return self;
}

- (void)reachabilityChanged:(NSNotification *)notification {
    
    NSNumber *s = notification.userInfo[AFNetworkingReachabilityNotificationStatusItem];
    AFNetworkReachabilityStatus status = [s integerValue];
    self.status = status;
    
    if (status == AFNetworkReachabilityStatusNotReachable) {
        [self.requestSerializer setCachePolicy:NSURLRequestReturnCacheDataElseLoad];
    }
    else {
        [self.requestSerializer setCachePolicy:NSURLRequestUseProtocolCachePolicy];
    }
}

- (BOOL)isConnected {
    
    if (self.status == AFNetworkReachabilityStatusNotReachable) {
        return NO;
    }
    
    return YES;
}


@end
