//
//  Show.m
//  recrutamento-ios
//
//  Created by Jean Vinge on 13/07/15.
//  Copyright (c) 2015 Jean Vinge. All rights reserved.
//

#import "ShowClient.h"
#import "recrutamento_ios-Swift.h"
#import "Client.h"

@implementation ShowClient

+ (void)returnPopularShowsWithCompletion:(void (^)(NSArray *list))completion failure:(void (^)(NSError *error))failure {
    
    [[Client sharedClient] GET:@"shows/popular" parameters:@{@"extended":@"images"} success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        Show *model = [[Show alloc] init];
        completion([model parseListOfShows:responseObject]);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        failure(error);
    }];
}

@end
