//
//  ShowCollectionViewController.swift
//  recrutamento-ios
//
//  Created by Jean Vinge on 09/07/15.
//  Copyright (c) 2015 Jean Vinge. All rights reserved.
//

import UIKit
import Kingfisher

let kShowCellIdentifier = "showCell"

class ShowCollectionViewController: UICollectionViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    var showArray:NSArray = []
    var refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        
        createRefreshControl()
        loadShowList()
    }
    
    func loadShowList() {
        ShowClient.returnPopularShowsWithCompletion({ (array) -> Void in
            
            self.refreshControl.endRefreshing()
            self.collectionView?.reloadData()
            self.showArray = array
            
            }, failure: { (error) -> Void in
                
                
        })
    }
    
    func createRefreshControl() {
        
    refreshControl = UIRefreshControl()
    refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
    refreshControl.addTarget(self, action: "refresh:", forControlEvents: UIControlEvents.ValueChanged)
    collectionView!.addSubview(refreshControl)
    }
    
    func refresh(sender:AnyObject) {
        loadShowList()
    }

    // Mark - UICollectionView Delegate/Datasource
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return showArray.count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        var cell = collectionView.dequeueReusableCellWithReuseIdentifier(kShowCellIdentifier, forIndexPath: indexPath) as! ShowCustomCollectionCell
        
        let show = showArray.objectAtIndex(indexPath.row) as! Show
        
        cell.coverTitle.text = show.showTitle;
        cell.coverImageView.kf_setImageWithURL(show.showImageUrl!)
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(8, 8, 8, 8)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        let width = collectionView.bounds.size.width / 3 - 16;
        let height = collectionView.bounds.size.height / 2.6;
        
        return CGSizeMake(width, height)
    }
    
}